angular.module('js.snippets').directive('script', function() {
  return {
    scope: false,
    restrict: 'AE',
    link: function(scope, element, attrs) {
      new Function(element.text())(scope);
    }
  }
});
