angular.module('js.snippets').provider('router', [
'$stateProvider', '$urlRouterProvider', '$locationProvider',
function($stateProvider, $urlRouterProvider, $locationProvider) {

  function each(arr, fn) {
      if (!arr || !fn) { return arr; }
      for (var i = 0; i < arr.length; i++) {
          fn.call(arr[i], arr[i], i);
      }
      return arr;
  }

  function map(arr, fn) {
      if (!arr || !fn) { return []; }
      var result = [];
      for (var i = 0; i < arr.length; i++) {
          result[result.length] = fn.call(arr[i], arr[i], i);
      }
      return result;
  }

  function keys(obj) {
      var result = [];
      if (obj) {
          for (var k in obj) {
              result.push(k);
          }
      }
      return result;
  }

  function State(name, url, template, controller, children, views) {
    this.name = name;
    this.url = url === '' ? '' : url || '/' + name;
    this.template = template || name;
    this.controller = controller || template || name;
    this.children = children || [];
    this.views = views;
  }

  function StateMaker(options, provider) {
    var self = this;
    provider = provider || $stateProvider;
    options = options || {};
    options.templatePrefix = options.templatePrefix || "views/";
    options.templateSuffix = options.templateSuffix || ".view.html";
    options.controllerPrefix = options.controllerPrefix || "";
    options.controllerSuffix = options.controllerSuffix || ".controller";

    this.inject = function(states) {
      each(states, function(state) {

        if (typeof state == 'string') {
            state = new State(state);
        }

        var actual = { url: state.url };

        if (state.views) {
          actual.views = state.views;
          each(keys(actual.views), function(key) {
            actual.views[key] = {
              templateUrl: options.templatePrefix + actual.views[key].templateUrl + options.templateSuffix,
              controller: options.controllerPrefix + actual.views[key].controller + options.controllerSuffix
            };
          });
        } else {
          actual.templateUrl = options.templatePrefix + state.template + options.templateSuffix;
          actual.controller = options.controllerPrefix + state.controller + options.controllerSuffix;
        }

        provider.state(state.name, actual);

        if (state.children.length) {
          self.inject(map(state.children, function(child) {
            return new State(state.name + '.' + child.name, child.url, child.template, child.controller, child.children, child.views);
          }));
        }
      });
    };
  }

  return (self = {
    injector: function(options) {
      return new StateMaker(options);
    },
    state: function(name, url, template, controller, children) {
      return new State(name, url, template, controller, children);
    },
    addStates: function(states) {
        self.injector().inject(states);
        return self;
    },
    stateWithChildren: function(name, children) {
      return new State(name, false, false, false, children);
    },
    stateWithViews: function(name, views) {
      return new State(name, false, false, false, false, views);
    },
    setDefault: function(name) {
        $urlRouterProvider.otherwise(name);
        return self;
    },
    html5Mode: $locationProvider.html5Mode,
    $get: [ function($stateProvider) {
      return self;
    }]
  });
}]);
