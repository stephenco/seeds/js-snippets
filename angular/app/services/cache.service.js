angular.module('js.snippets').factory('cache', ['utils',
    function (utils) {

        /// Describes a value stored in cache.
        function CacheItem(value, options) {
            var self = this;
            options = options || {};
            options.value = value;

            /// Gets or sets the value of this item.
            this.value = function () {
                if (!arguments.length) {
                    return options.value;
                }
                options.value = arguments[0];
                return self;
            };

            /// Indicates whether or not this item is currently active (vs. expired)
            this.isActive = function () {
                var expires = self.expires();
                return !expires || expires > new Date();
            };

            /// Gets or sets the date at which this value expires in cache.
            /// An undefined expiration date means the value will live for the life of its container.
            this.expires = function () {
                if (!arguments.length) {
                    return options.expires;
                }
                var d = arguments[0];
                options.expires = (d && d instanceof Date) ? d : self.undefined;
                return self;
            };
        }

        var cache = {};
        var service = {

            /// Gets a stored value from cache.
            get: function (key, fnDefault) {
                if (cache[key] && cache[key].isActive()) {
                    return cache[key].value();
                }
                return utils.isFunction(fnDefault) ? fnDefault() : service.undefined;
            },

            /// Sets new values into the cache.
            /// Set options.expire to a Date value to avoid keeping it around forever.
            /// Returns the value being set so that it may be used in-line.
            set: function (key, value, options) {
                cache[key] = new CacheItem(value, options);
                return value;
            },

            // Removes a given key from the list of cached values.
            drop: function(key) {
                if (cache[key]) {
                    delete cache[key];
                }
                return service;
            },

            /// Removes expired values.
            clean: function () {
                utils.eachKey(cache, function (key) {
                    if (cache[key] && !cache[key].isActive()) {
                        delete cache[key];
                    }
                });
                return service;
            }
        };

        // return the service and run garbage collection every couple minutes
        setInterval(service.clean, 2000);
        return service;
    }
]);
