angular.module('js.snippets').service('csv', ['$http', '$q', 'utils', 'cache',
    function($http, $q, utils, cache) {
        var self = this;

        /// http://stackoverflow.com/questions/1293147/javascript-code-to-parse-csv-data
        this.fromString = function(strData, strDelimiter) {
            strDelimiter = (strDelimiter || ",");
            var objPattern = new RegExp(
                "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" + // delimiters
                "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" + // quoted
                "([^\"\\" + strDelimiter + "\\r\\n]*))", // normal
                "gi"
            );
            var arrData = [[]];
            var arrMatches = null;
            while ((arrMatches = objPattern.exec(strData))) {
                var strMatchedDelimiter = arrMatches[1];
                if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
                    arrData.push([]);
                }
                var strMatchedValue;
                if (arrMatches[2]) {
                    strMatchedValue = arrMatches[2].replace(new RegExp("\"\"", "g"), "\"");
                } else {
                    strMatchedValue = arrMatches[3];
                }
                arrData[arrData.length - 1].push(strMatchedValue);
            }
            return (arrData);
        };

        this.get = function(filename, force) {
            var key = '--csv-promise-' + filename;
            if (cache.get(key) && !force) {
                return cache.get(key).promise;
            }

            var deferred = $q.defer();
            cache.set(key, deferred);

            $http.get(filename).then(function(result) {
                //self.fromString(result.data);
                try {
                    var lines = result.data.split('\n');
                    var properties = utils.map(lines.shift().split(','), function() {
                        return this.trim().replace(/\s+/, '_').replace(/[\.]/g, '').toLowerCase();
                    });

                    var csv = utils.map(self.fromString(lines.join('\n')), function(values) {
                        var item = {};
                        utils.each(properties, function(prop, i) {
                            item[prop] = values[i];
                        });
                        return item;
                    });

                    deferred.resolve(csv);
                } catch (err) {
                    deferred.reject(err);
                }
            }, function(error) {
                $q.reject(error);
            });

            return deferred.promise;
        };

    }
]);
