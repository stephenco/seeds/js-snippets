angular.module('js.snippets').factory('extensionCore', [ 'generate', 'utils',
    function(gen, utils) {

        var service = {
            Extension: function(name, fn) {
                this.name = gen.getter(name);
                this.fn = gen.getter(fn);
                this.isExtension = true;
            },

            ExtensionsCollection: function(list) {
                gen.inject(this).addListProperty("extension", "extensions", list); // TODO: assoc. array is better for this
                var self = this;
                var catchAllTag = "__catchAll";

                /// Registers a 'catch-all' function which may be invoked when no specialized handler is provided.
                this.addCatchAll = function (fn) {
                    self.addExtension(new service.Extension(catchAllTag, fn));
                    return self;
                };

                /// Finds a specialized exension handler by name, or returns the catch-all, if any.
                this.find = function (name) {
                    var list = self.extensions();

                    // look through the list of registered extensions for the given name, first
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].name() == name) {
                            return list[i].fn();
                        }
                    }

                    // then look through the whole list again for a catch-all, if the name is not found
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].name() == catchAllTag) {
                            return list[i].fn();
                        }
                    }

                    // TODO: clearly optimizations could be made here.
                    // Perhaps an associative array instead of looping, and also we could just save a
                    // reference to the catch-all instead of digging for it, etc.

                    return null;
                };

                /// Invokes a specialized extension handler by name, or the catch-all, if any, given context
                /// and an object describing any additional dependencies (as appropriate).
                this.invoke = function(name, context, dependencies) {
                    var fn = self.find(name);
                    if (fn) {
                        fn.apply(context || self, dependencies);
                    }
                    return self;
                };
            }
        };

        return service;
    }
]);
