angular.module('js.snippets').factory('generate', [ 'utils',
    function(utils) {

        var service = {
            getter: function(value) {
                return function() {
                    return value;
                };
            },

            getterSetter: function(self, value) {
                return function(x) {
                    if (arguments.length < 1) {
                        return value;
                    }
                    value = x;
                    return self;
                };
            },

            addToListSetter: function(self, list) {
                return function(value) {
                    if (value) {
                        list[list.length] = value;
                    }
                    return self;
                };
            },

            removeFromListSetter: function(self, list) {
                return function(value) {
                    if (value) {
                        var i = list.indexOf(value);
                        if (i >= 0) {
                            list.splice(i, 1);
                        }
                    }
                    return self;
                };
            },

            inject: function(self) {
                return new(function Injector() {
                    var injector = this;

                    this.addGetter = function(name, defaultValue) {
                        var obj = defaultValue || null;
                        self[name] = service.getter(obj);
                        return injector;
                    };

                    this.addGetterSetter = function(name, defaultValue) {
                        var obj = defaultValue || null;
                        self[name] = service.getterSetter(self, obj);
                        return injector;
                    };

                    this.addListProperty = function(name, namePlural, defaultValue) {
                        var list = defaultValue || [];
                        self[namePlural ? namePlural : (name + 's')] = service.getter(list);
                        self["add" + utils.ucaseFirst(name)] = service.addToListSetter(self, list);
                        self["remove" + utils.ucaseFirst(name)] = service.removeFromListSetter(self, list);
                        return injector;
                    };
                })();
            }
        };

        return service;
    }
]);
