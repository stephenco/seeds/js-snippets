angular.module('js.snippets').factory('logger', [ 'utils', 'generate',
    function (helpers, gen) {

        function Logger(options) {
            var self = this, _history = [], _levels = {
                verbose: 0,
                info: 1,
                log: 2,
                trace: 2,
                warn: 3,
                error: 4,
                exception: 5
            };

            options = options || {};
            gen.inject(this)
                .addGetterSetter('enabled', options.enabled || true)
                .addGetterSetter('keep', options.keep || 100)
                .addGetterSetter('level', options.level || _levels.info);

            function keepHistory(type, args) {
                _history.push({ type: type, time: new Date(), args: args });
                if (_history.length > self.keep()) {
                    history = _history.slice(self.keep() - _history.length);
                }
            }

            function canShow(level) {
                return self.enabled() && _levels[level] >= self.level();
            }

            /// Returns the list of available log levels.
            this.levels = function() { return _levels; }

            /// Returns the list of tracked history for this logger.
            this.history = function() { return _history; }

            /// Clears the tracked history for this logger.
            this.clear = function() {
                _history = [];
                return self;
            };

            /// Dumps a report of the tracked history to console.log.
            this.report = function() {
                helpers.each(_history, function(line) {
                    var arr = [].slice.call(line.args);
                    arr.push(line.type);
                    arr.push(line.time);
                    console.log.call(self, arr);
                });
            }

            // Add logger functions wrapping their counterparts on console.
            // All logger functions return their first parameter in order
            // to simplify using logging in-line with other code.
            helpers.each(['log', 'info', 'warn', 'error', 'exception', 'trace'], function(level) {
                self[level] = function() {
                    keepHistory(level, arguments);
                    if (canShow(level)) {
                        console[level].apply(self, arguments);
                    }
                    return arguments[0];
                };
            });

            /// Adds support for verbose level logging.
            this.verbose = function() {
                keepHistory('verbose', arguments);
                if (canShow('verbose')) {
                    console.log.apply(self, arguments);
                }
                return arguments[0];
            };

            /// Creates a new copy of this logger using current settings,
            /// but not including history.
            this.copy = function() {
                return new Logger({
                    enabled: self.enabled(),
                    keep: self.keep(),
                    level: self.level()
                });
            };
        }

        var service = new Logger();
        window.getLogger = function() { return service; };

        return service;
    }
]);
