angular.module('js.snippets').factory('moment', [ "$window",
    function ($window) {
        var service = $window.moment || { missing: true };
        if (service.missing) {
            console.warn("Moment.js is not available.");
        }
        return service;
    }
]);
