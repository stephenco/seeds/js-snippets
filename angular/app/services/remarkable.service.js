angular.module('js.snippets').factory('remarkable', [ "$window",
    function ($window) {
        var service = $window.Remarkable ? new $window.Remarkable({
            breaks: true,
            xhtmlOut: true,
            linkify: true
        }) : { missing: true };
        if (service.missing) {
            console.warn("remarkable.js is not available.");
        }
        return service;
    }
]);
