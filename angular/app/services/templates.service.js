angular.module('js.snippets').service('templates', [ "$q", "$http", "$compile", "$templateCache",
    function($q, $http, $compile, $templateCache) {

        this.applyTemplate = function(url, elTarget, scope, fnGet) {
            var deferred = $q.defer();

            function append(markup) {
                elTarget.append($compile(markup)(scope));
                return deferred.resolve(markup);
            }

            var markup = $templateCache.get(url);
            if (markup) {
                return append(markup);
            }

            (fnGet || $http.get)(url).then(function(result) {
                markup = result.data || "";
                $templateCache.put(url, markup);
                append(markup);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred;
        };
    }
]);
