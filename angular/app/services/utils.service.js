angular.module('js.snippets').factory('utils', [
    function () {
        'use strict';

        var service = {

            /// Takes a string and returns a version in which the first character is upper-case.
            ucaseFirst: function (str) {
                return (str && str.length) ? (str.charAt(0).toUpperCase() + str.slice(1)) : str;
            },

            /// Invokes a given function for each element in a list and returns the list.
            /// 'fn' is invoked in the 'this' context of each item, and the item will be passed as the first parameter.
            /// The index of the item in the list will be passed as the second. (Handlers need not observe arguments if
            /// they prefer to work over 'this' internally.)
            each: function (arr, fn) {
                if (!arr || !fn) { return arr; }
                for (var i = 0; i < arr.length; i++) {
                    fn.call(arr[i], arr[i], i);
                }
                return arr;
            },

            /// Returns a NEW list containing a subset of a given list by invoking a given function for each item in the
            /// list and including it if the resulting value is truthy. 'fn' is invoked in the 'this' context of each item,
            /// and the item will be passed as the first parameter. The index of the item in the original list will be passed
            /// as the second. (Handlers need not observe arguments if they prefer to work over 'this' internally.)
            select: function (arr, fn) {
              if (!arr || !fn) { return arr; }
              var result = [];
              for (var i = 0; i < arr.length; i++) {
                  if (fn.call(arr[i], arr[i], i)) {
                      result[result.length] = arr[i];
                  }
              }
              return result;
            },

            /// Backwards select - includes only items that don't match the filter.
            except: function(arr, fn) {
              if (!arr || !fn) { return arr; }
              var result = [];
              for (var i = 0; i < arr.length; i++) {
                  if (!fn.call(arr[i], arr[i], i)) {
                      result[result.length] = arr[i];
                  }
              }
              return result;
            },

            /// Returns the first item in a list that matches the given filter function (by producing a truthy value).
            first: function (arr, fn) {
                if (!arr || !fn) { return null; }
                for (var i = 0; i < arr.length; i++) {
                    if (fn.call(arr[i], arr[i], i)) {
                        return arr[i];
                    }
                }
                return null;
            },

            /// Invokes a given function for each element in a list and returns a NEW list, using the output of each invocation.
            /// 'fn' is invoked in the 'this' context of each item, and the item will be passed as the first parameter.
            /// The index of the item in the list will be passed as the second. (Handlers need not observe arguments if
            /// they prefer to work over 'this' internally.)
            map: function (arr, fn) {
                if (!arr || !fn) { return []; }
                var result = [];
                for (var i = 0; i < arr.length; i++) {
                    result[result.length] = fn.call(arr[i], arr[i], i);
                }
                return result;
            },

            /// Returns a list of all properties (keys) for a given object.
            keys: function (obj) {
                var result = [];
                if (obj) {
                    for (var k in obj) {
                        result.push(k);
                    }
                }
                return result;
            },

            /// Executes a function for each property (key) on an object.
            /// 'fn' is invoked in the 'this' context of each key, given the key as the first parameter.
            eachKey: function(obj, fn) {
                return service.each(service.keys(obj), fn);
            },

            /// Copies the properties from one object into another or creates a
            /// new object with the properties of the first if no second object is given.
            /// You can omit properties by passing names in the 'except' array.
            copyProperties: function(obj, outp, except) {
              outp = outp || {};
              except = except || [];
              var keys = services.except(service.keys(obj), except);
              service.each(keys, function(k) { outp[k] = obj[k]; });
              return obj;
            },

            /// Inspects an object and indicates whether or not it appears be be undefined.
            isDefined: function isDefined(obj) {
              return typeof someVar !== 'undefined';
            },

            /// Inspects an object and indicates whether or not it can be invoked as a function.
            isFunction: function (obj) {
                return obj && {}.toString.call(obj) === '[object Function]';
            },

            /// Inspects an object and indicates whether or not it appears to be a valid date object.
            isValidDate: function (value) {
                return (Object.prototype.toString.call(value) === "[object Date]") && !isNaN(value.getTime());
            },

            /// Inspects an object and indicates whether or not it appears to be a valid numeric value.
            isNumeric: function (n) {
                return !isNaN(parseFloat(n)) && isFinite(n);
            },

            /// Inspects an object and indicates whether or not it appears to be a boolean value.
            isBoolean: function(obj) {
                return obj === true || obj === false;
            },

            /// Writes an XML DOM log line to the debug console given an XML string value.
            logXml: function (str) {
                console.log(new DOMParser().parseFromString(str, "text/xml"));
                return service;
            },

            createRandomId: function (size, chars) {
                var result = "";

                // by default, the array of available characters come from an array of
                // numbers in the current timestamp (or just use what was given)
                chars = (chars || (+new Date()).toString()).split("");

                // get a character for each slot in the result string
                for (var i = 0; i < (size || 15); ++i) {
                    var min = 0, max = size - 1;
                    var at = Math.floor(Math.random() * (max - min + 1)) + min;
                    if (at > chars.length) {
                        at = chars.length - at;  // wrap around if needed
                    }
                    id += ts[at];
                }

                return id;
            },

            getDateAfterSeconds: function (n) {
                var d = new Date();
                d.setSeconds(d.getSeconds() + n);
                return d;
            }
        };

        return service;
    }
]);
