angular.module('js.snippets').factory('xmlWriterService', [ 'utils', 'generate',
    function (utils, gen) {

        function sanitize(str) {
            return str;
        }

        var service = {
            Attribute: function (name, value) {
                if (utils.isBoolean(value) || utils.isNumeric(value)) {
                    value = value.toString();
                }

                gen.inject(this)
                    .addGetterSetter("name", name)
                    .addGetterSetter("value", value);

                var self = this;

                this.toStr = function() {
                    return sanitize(self.name()) + "=\"" + sanitize(self.value()) + "\"";
                };
            },

            Tag: function(name, value) {
                if (utils.isBoolean(value) || utils.isNumeric(value)) {
                    value = value.toString();
                }

                gen.inject(this)
                    .addGetterSetter("name", name)
                    .addGetterSetter("value", value)
                    .addListProperty("attr")
                    .addListProperty("child", "children");

                var self = this;

                this.to = function(tag) {
                    tag.addChild(self);
                    return self;
                };

                this.toStr = function() {
                    var name = sanitize(self.name());
                    var str = "<" + name;

                    utils.each(self.attrs(), function(attr) {
                        str += " " + attr.toStr();
                    });

                    if (self.children().length) {
                        str += ">";
                        utils.each(self.children(), function(child) {
                            str += child.toStr();
                        });
                        str += (self.value() ? sanitize(self.value()) : "") + "</" + name + ">";
                    } else {
                        str += self.value() ? (">" + sanitize(self.value()) + "</" + name + ">") : " />";
                    }

                    return str;
                };
            }
        };

        return service;
    }
]);
